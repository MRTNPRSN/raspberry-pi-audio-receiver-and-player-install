#!/bin/sh
### BEGIN INIT INFO
# Provides:             mrss_daemon.sh
# Required-Start:       $network $named
# Required-Stop:
# Default-Start:        4 5
# Default-Stop:         0 1 6
# Short-Description:    starting raspberry pi internet multi room sound system satellite
# Description:          Start the program /usr/share/mrss/hardware_control_daemon.py
#                       to control system services by hardware buttons and monitor functionality
#                       by LED's
### END INIT INFO

DAEMON="/usr/share/mrss/mrss_daemon.py"
PID_FILE="/var/run/mrss-satellite.pid"

case "$1" in
	start)
		echo "starting MRSS Satellite"
		# Start the daemon
		python3 ${DAEMON} start
		;;
	restart)
		echo "restarting MRSS Satellite"
		# Restart daemon
		python3 ${DAEMON} restart
		;;
	stop)
		echo "stopping MRSS Satellite"
		# Stop daemon
		python3 ${DAEMON} stop
		;;
	status)
		if [ -f ${PID_FILE} ]; then
		    echo "MRSS Satellite is running"
		    python3 ${DAEMON} status
		else echo "MRSS is stopped"
		fi
		;;
	*)
		# Print usage and refuse other arguments
		echo "Usage: $0 {start|stop|restart}"
		exit 1
		;;
esac

exit 0