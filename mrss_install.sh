#!/usr/bin/env bash

function tst {
    echo "===> Executing: $*"
    if ! $*; then
        echo "Exiting script due to error from: $*"
        exit 1
    fi
}

# install required python modules
tst sudo pip3 install daemons
tst sudo apt-get install python3-rpi.gpio

# install mrss files
tst sudo install -d /usr/share/mrss
tst sudo install -D mrss/hardware_control_daemon.py /usr/share/mrss
tst sudo install -D mrss/mrss_daemon.py /usr/share/mrss
tst sudo install -D init.d/mrss_daemon.sh /etc/init.d

# set file permissions
tst sudo chown root:root /etc/init.d/mrss_daemon.sh
tst sudo chmod 755 /etc/init.d/mrss_daemon.sh

# enable service
tst sudo systemctl enable mrss_daemon.service
