#!/usr/bin/env python3

import logging
import os
import sys

from hardware_control_daemon import HardwareControlDaemon

if __name__ == "__main__":
    action = sys.argv[1]
    logfile = os.path.join("/var/log", "mrss-satellite.log")
    pidfile = os.path.join("/var/run", "mrss-satellite.pid")

    logging.basicConfig(filename=logfile, level=logging.DEBUG)
    d = HardwareControlDaemon(pidfile=pidfile)

    if action == "start":
        d.start()
    elif action == "stop":
        d.stop()
    elif action == "restart":
        d.restart()
    elif action == "status":
        d.status()
