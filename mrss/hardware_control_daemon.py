#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import RPi.GPIO as GPIO
import time
import subprocess
from daemons.prefab import run


class HardwareControlDaemon(run.RunDaemon):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)

        # setup
        GPIO.setmode(GPIO.BOARD)
        GPIO.setup(7, GPIO.IN)

        self.led_gpio_array = [11, 13, 15]
        for led in self.led_gpio_array:
            GPIO.setup(led, GPIO.OUT)

        self.services_map = ['mpd', 'bluetooth', 'shairport-sync']

        self.pointer = 0

    def init_led(self):
        for led in self.led_gpio_array:
            self.led_on(led)
        time.sleep(1.5)
        for led in self.led_gpio_array:
            self.led_off(led)

    def init_services(self):
        for service in range(1, len(self.services_map) - 1):
            self.services_down(self.services_map[service])

    @staticmethod
    def services_down(service):
        if(subprocess.call(["sudo", "service", service, "stop"])) == 0:
            return True
        else:
            return False

    @staticmethod
    def service_up(service):
        if service == 'bluetooth':
            r = subprocess.call(["sudo", "service", service, "start"])
            r += subprocess.call(["sudo", "service", "bluetooth-agent", "start"])
            if r == 0:
                return True
            else:
                return False
        elif(subprocess.call(["sudo", "service", service, "start"])) == 0:
            return True
        else:
            return False

    @staticmethod
    def led_off(led):
        GPIO.output(led, GPIO.LOW)

    @staticmethod
    def led_on(led):
        GPIO.output(led, GPIO.HIGH)

    def all_services_down(self):
        for service in self.services_map:
            self.services_down(service)

    def all_led_off(self):
        for led in self.led_gpio_array:
            self.led_off(led)

    def status(self):
        print("{0} running".format(self.services_map[self.pointer]))

    def run(self):
        _run_ = True
        trigger = False
        system_is_up = True
        _time_ = 0
        self.init_services()
        self.init_led()
        self.led_on(self.led_gpio_array[self.pointer])

        while _run_ is True:
            if GPIO.input(7) == GPIO.LOW and trigger is False:
                trigger = True
                _time_ = time.time()
            elif GPIO.input(7) == GPIO.LOW and trigger is True:
                if int(time.time() - _time_) >= 2:
                    if system_is_up is True:
                        self.all_led_off()
                        self.all_services_down()
                        system_is_up = False
                    elif system_is_up is False:
                        self.init_services()
                        self.init_led()
                        system_is_up = True

            if GPIO.input(7) == GPIO.HIGH and trigger is True:
                if system_is_up is True:
                    self.led_off(self.led_gpio_array[self.pointer])
                    self.services_down(self.services_map[self.pointer])
                    if self.pointer == len(self.led_gpio_array) - 1:
                        self.pointer = 0
                    else:
                        self.pointer += 1

                    self.led_on(self.led_gpio_array[self.pointer])
                    self.service_up(self.services_map[self.pointer])
                    trigger = False
            time.sleep(0.05)
