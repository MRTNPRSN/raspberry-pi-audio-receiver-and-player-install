This is a fork sebastian_jj's Raspberry Pi Audio Receiver. 

He have added mpd installation / configuration to play internet radio and local stored music.

A python daemon will be installed to switch between mpd, bluetooth or airplay, just press a button installed on GPIO. The current source is indicated by LED's.

I simply modified this to run smoothly on raspbian jessie lite (2017-04-10)