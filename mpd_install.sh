#!/bin/bash

read -p "Device name: " DEV_NAME

function tst {
    echo "===> Executing: $*"
    if ! $*; then
        echo "Exiting script due to error from: $*"
        exit 1
    fi
}

# update
tst sudo apt-get update
tst sudo apt-get -y dist-upgrade

# cleaning
tst sudo apt-get -y remove libreoffice*
tst sudo apt-get -y remove scratch
tst sudo apt-get -y remove wolfram*
tst sudo apt-get -y remove bluej
tst sudo apt-get -y remove nodered
tst sudo apt-get -y remove minecraft-pi
tst sudo apt-get -y remove sonic-pi
tst sudo apt-get autoremove

# install mpd and mpc
tst sudo apt-get -y install mpd
tst sudo apt-get -y install mpc
tst sudo systemctl enable mpd
tst sudo cp -vf mpd.conf /etc

# create users and privileges - mostly should already exists
tst sudo addgroup --system pulse
tst sudo adduser --system --ingroup pulse --home /var/run/pulse pulse
tst sudo addgroup --system pulse-access
tst sudo usermod -a -G pulse mpd
tst sudo usermod -a -G pulse-access mpd

# configure system
tst sudo echo ${DEV_NAME} > /etc/hostname

# configure mpd
sudo patch /etc/mpc.conf << EOT
--- mpd.conf	2014-12-06 00:00:00.000000000 +0100
+++ /etc/mpd.conf	2017-04-13 05:28:47.949469690 +0200
@@ -72,7 +72,7 @@
 # This is useful if MPD needs to be a member of group such as "audio" to
 # have permission to use sound card.
 #
-#group                          "nogroup"
+group                          "audio"
 #
 # This setting sets the address for the daemon to listen on. Careful attention
 # should be paid if this is assigned to anything other then the default, any.
@@ -80,7 +80,8 @@
 # to have mpd listen on every address
 #
 # For network
-bind_to_address		"localhost"
+#bind_to_address		"127.0.0.1"
+bind_to_address		"${DEV_NAME}"
 #
 # And for Unix Socket
 #bind_to_address		"/run/mpd/socket"
@@ -153,12 +154,12 @@
 # If this setting is set to "yes", service information will be published with
 # Zeroconf / Avahi.
 #
-#zeroconf_enabled		"yes"
+zeroconf_enabled		"yes"
 #
 # The argument to this setting will be the Zeroconf / Avahi unique name for
 # this MPD server on the network.
 #
-#zeroconf_name			"Music Player"
+zeroconf_name			"${DEV_NAME}"
 #
 ###############################################################################
EOT

sleep 5
echo "Done! You should reboot now"